package com.pettt.hman.fxcomponents.namespacefilter;

import com.pettt.hman.fxcomponents.imagebutton.ImageButton;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;


public class RangeFilterBox extends BorderPane {

    private final TextField minFilter;
    private final TextField maxFilter;

    public RangeFilterBox(){
        this("range filter","unit");
    }

    public RangeFilterBox(String name, String unit){
        getStylesheets().add(NamespaceFilter.class.getResource( "namespacefilter.css").toExternalForm());

        Label description = new Label();
        description.setEffect(new InnerShadow(4.0,Color.ORANGE));
        description.getStyleClass().add("list-descriptor");
        description.setText(name);



        //clear button
        ImageButton clear = new ImageButton();
        clear.getStyleClass().add("list-selection-clear");


        StackPane top = new StackPane();
        StackPane.setAlignment(description, Pos.CENTER_LEFT);
        StackPane.setAlignment(clear, Pos.CENTER_RIGHT);
        top.getChildren().addAll(description, clear);
        setTop(top);

        //range filters
        minFilter = new TextField();
        minFilter.setPromptText("Min ("+unit+ ") ...");
        minFilter.getStyleClass().add("list-text-filter");
        minFilter.textProperty().addListener(obs -> {
            if(minFilter.getText().isEmpty()) return;
            try {
                //noinspection ResultOfMethodCallIgnored
                Float.valueOf(minFilter.getText());
            } catch (Exception e) {
                Platform.runLater(minFilter::clear);
            }
        });
        setCenter(minFilter);

        maxFilter = new TextField();
        maxFilter.setPromptText("Max ("+unit+ ") ...");
        maxFilter.getStyleClass().add("list-text-filter");
        maxFilter.textProperty().addListener(obs -> {
            if(maxFilter.getText().isEmpty()) return;
            try {
                //noinspection ResultOfMethodCallIgnored
                Float.valueOf(maxFilter.getText());
            } catch (Exception e) {
                Platform.runLater(maxFilter::clear);
            }
        });
        setBottom(maxFilter);

        description.effectProperty().bind(Bindings.when(
                Bindings.isNotEmpty(minFilter.textProperty()).or(Bindings.isNotEmpty(maxFilter.textProperty())))
                        .then(new InnerShadow(4.0, Color.ORANGE))
                        .otherwise((InnerShadow) null)
        );
        clear.setOnAction(e -> {
            Platform.runLater(maxFilter::clear);
            Platform.runLater(minFilter::clear);
        });
    }

    public String getMinFilter(){
        return minFilter.getText();
    }

    public String getMaxFilter(){
        return maxFilter.getText();
    }

    public void setMinFilter(String min){
        minFilter.setText(min);
    }

    public void setMaxFilter(String max){
        maxFilter.setText(max);
    }

    public StringProperty minFilterProperty(){
        return minFilter.textProperty();
    }

    public StringProperty maxFilterProperty(){
        return maxFilter.textProperty();
    }
}
