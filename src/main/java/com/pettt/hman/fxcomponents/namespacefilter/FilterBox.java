package com.pettt.hman.fxcomponents.namespacefilter;

import com.pettt.hman.fxcomponents.imagebutton.ImageButton;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

import java.util.Comparator;


public class FilterBox extends BorderPane {

    private final NamespaceFilter filterList;

    private final ObservableList<String> data = FXCollections.observableArrayList();

    public FilterBox(){
        this("namespace","namespaces");
    }

    public FilterBox(String name, String plural){
        getStylesheets().add(NamespaceFilter.class.getResource( "namespacefilter.css").toExternalForm());

        SortedList<String> sortedData = new SortedList<>(data);
        sortedData.setComparator(String::compareToIgnoreCase);
        FilteredList<String> filteredData = new FilteredList<>(sortedData, s -> true);

        filterList = new NamespaceFilter(name,plural,filteredData);

        TextField filterInput = new TextField();
        filterInput.setPromptText("Filter...");
        filterInput.getStyleClass().add("list-text-filter");
        filterInput.textProperty().addListener(obs -> {
            String filter = filterInput.getText();
            if (filter == null || filter.length() == 0) {
                filteredData.setPredicate(s -> true);
            } else {
                filteredData.setPredicate(s -> s.contains(filter));
            }
        });

        Label description = new Label();
        description.setEffect(new InnerShadow(4.0,Color.ORANGE));
        description.getStyleClass().add("list-descriptor");
        StringProperty desc = new SimpleStringProperty(name+" (");
        description.textProperty().bind(desc.concat(Bindings.size(data)).concat(")"));


        description.effectProperty().bind(Bindings.when(
                Bindings.isNotEmpty(getList().getSelectionModel().getSelectedItems()))
                .then(new InnerShadow(4.0, Color.ORANGE))
                .otherwise((InnerShadow) null)
        );

        //clear button
        ImageButton clear = new ImageButton();
        clear.getStyleClass().add("list-selection-clear");
        clear.setOnAction(e -> getList().getSelectionModel().clearSelection());


        StackPane top = new StackPane();
        StackPane.setAlignment(description, Pos.CENTER_LEFT);
        StackPane.setAlignment(clear, Pos.CENTER_RIGHT);
        top.getChildren().addAll(description, clear);
        setTop(top);
        setCenter(filterList);
        setBottom(filterInput);
    }

    public NamespaceFilter getList() {
        return filterList;
    }

    public ObservableList<String> getData(){
        return data;
    }
}
