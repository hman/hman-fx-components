package com.pettt.hman.fxcomponents.namespacefilter;

import javafx.application.Application;
import javafx.beans.property.BooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
//import org.kohsuke.randname.RandomNameGenerator;

import javax.naming.Name;
import java.util.Random;


public class Test extends Application{

    @Override
    public void start(Stage stage) throws Exception {
        BorderPane root = new BorderPane();
        root.setStyle("-fx-background-color: gray");


        ObservableList<String> items = FXCollections.observableArrayList();
        FilterBox filter = new FilterBox("artist", "artists");
        filter.getData().addAll("A coll", "G coll", "Z coll");
        filter.getData().addAll("S coll", "G coll", "Z coll");
        filter.getData().addAll("A coll", "G coll", "Z coll");
        filter.getData().addAll("A coll", "G coll", "Z coll");
        filter.getData().addAll("A coll", "G coll", "Z coll");



        RangeFilterBox b = new RangeFilterBox("Date", "days old");
        StackPane center = new StackPane(filter);
        center.setStyle("-fx-background-color: dimgray");
        center.setMaxSize(400,400);


        TextField bottom = new TextField();


        root.setCenter(center);
        root.setBottom(bottom);

        Scene sc = new Scene(root);
        stage.setScene(sc);
        stage.show();





        fillRandomArtists(items);
    }

    private void fillRandomArtists(ObservableList<String> items) {
        //RandomNameGenerator rnd = new RandomNameGenerator(0);

        for (int i = 0; i < 100; i++) {
            items.add("ell");
        }
    }
}
