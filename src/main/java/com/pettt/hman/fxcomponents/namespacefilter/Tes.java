package com.pettt.hman.fxcomponents.namespacefilter;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.util.Comparator;

public class Tes extends Application
{
    public static final ObservableList names = FXCollections.observableArrayList();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        final ListView listView = new ListView(names);
        listView.setPrefSize(200, 250);
        listView.setEditable(true);

        names.addAll("Brenda", "Adam", "Williams", "Zach", "Connie", "Donny", "Lynne", "Rose", "Tony", "Derek");

        listView.setItems(names);
        SortedList<String> sortedList = new SortedList(names);
        sortedList.setComparator(new Comparator<String>(){
            @Override
            public int compare(String arg0, String arg1) {
                return arg0.compareToIgnoreCase(arg1);
            }
        });

        for(String s : sortedList)
            System.out.println(s);
        System.out.println();

        names.add("Foo");
        System.out.println("'Foo' added");
        for(String s : sortedList)
            System.out.println(s);

        StackPane root = new StackPane();
        root.getChildren().add(listView);
        primaryStage.setScene(new Scene(root, 200, 250));
        primaryStage.show();
    }
}

