package com.pettt.hman.fxcomponents.namespacefilter;

import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;


public class NamespaceFilter extends ListView<String> {
    protected StringProperty namespaceProperty = new SimpleStringProperty("Undefined");
    protected StringProperty namespacePluralProperty = new SimpleStringProperty("Undefined");

    public NamespaceFilter() {
        this("Undefined", "Undefined", FXCollections.observableArrayList());
    }


    public NamespaceFilter(String namespace, String plural, ObservableList<String> items){
        disableProperty().bind(Bindings.isEmpty(namespaceProperty));

        getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);


        namespaceProperty.set(namespace);
        namespacePluralProperty.set(plural);

        setItems(items);

        setCellFactory(this::createNamespaceCellFactory);
    }

    public NamespaceFilter(String filter) {
        this(filter,filter, FXCollections.observableArrayList());
    }

    protected ListCell<String> createNamespaceCellFactory(ListView<String> listView) {

        ListCell<String> cell = new ListCell<>();
        cell.prefWidthProperty().bind(listView.widthProperty().subtract(19));
        cell.setMaxWidth(Control.USE_PREF_SIZE);

        ContextMenu contextMenu = new ContextMenu();


        MenuItem editItem = new MenuItem();
        //editItem.setDisable(true);
        editItem.textProperty().bind(Bindings.format("Edit metadata"));
        editItem.setOnAction(event -> {
            ObservableList<String> items = listView.getSelectionModel().getSelectedItems();
            //System.out.println("Editing "+ items);
        });
        MenuItem deleteItem = new MenuItem();
        deleteItem.textProperty().bind(Bindings.format("Delete \"%s\"", cell.itemProperty()));
        deleteItem.setOnAction(event -> listView.getItems().remove(cell.getItem()));
        contextMenu.getItems().addAll(editItem);


        cell.textProperty().bind( cell.itemProperty());


        cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
            if (isNowEmpty) {
                cell.setContextMenu(null);
            } else {
                cell.setContextMenu(contextMenu);
            }
        });
        return cell ;
    }


    public String getNamespaceProperty() {
        return namespaceProperty.get();
    }

    public StringProperty namespacePropertyProperty() {
        return namespaceProperty;
    }

    public void setNamespaceProperty(String namespaceProperty) {
        this.namespaceProperty.set(namespaceProperty);
    }

    public String getNamespacePluralProperty() {
        return namespacePluralProperty.get();
    }

    public StringProperty namespacePluralPropertyProperty() {
        return namespacePluralProperty;
    }

    public void setNamespacePluralProperty(String namespacePluralProperty) {
        this.namespacePluralProperty.set(namespacePluralProperty);
    }
}
