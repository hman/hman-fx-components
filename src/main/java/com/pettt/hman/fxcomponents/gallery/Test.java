package com.pettt.hman.fxcomponents.gallery;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.controlsfx.control.GridCell;
import org.controlsfx.control.GridView;

/**
 * 2017/01/05.
 *
 * //TODO [Issue #4]
 */
public class Test extends Application{
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        try {
            VBox root = new VBox();
            Scene scene = new Scene(root,400,400);
            primaryStage.setScene(scene);

            root.setAlignment(Pos.CENTER);

            GalleryThumb gt2 = new GalleryThumb();
            gt2.setUnreadTitle(true);
            root.getChildren().add(gt2);

            GalleryThumb gt = new GalleryThumb();
            gt.setImage(new Image(Test.class.getResource("/test.png").toExternalForm()));
            gt.setNewTitle(true);
            gt.setFavTitle(true);
            gt.setUnreadTitle(true);
            root.getChildren().add(gt);


            //menuItems.setCellFactory();
            //menuItems.setCellFactory(new GalleryThumbCellFactory());

            primaryStage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}
