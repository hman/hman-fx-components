package com.pettt.hman.fxcomponents.gallery;

import com.pettt.hman.fxcomponents.imagebutton.ImageButton;
import com.pettt.hman.fxcomponents.ratingbar.RatingFilterBar;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;

import static com.pettt.hman.fxcomponents.gallery.GalleryThumb.BORDER_STYLE.EDGE_BORDER;


/**
 * A gallery display which displays a (default) cover, a title, and a origin label with tooltip.
 * <p>
 *     This component currently has fixed dimensions, 230px * 150px.
 * </p>
 *
 * //TODO dynamic size with FlowGridPane or similar (see gists) [Issue #1]
 * //TODO show only tooltip if text is overrun [Issue #2]
 * //TODO right ribbon if left overrun [Issue #3]
 * //TODO right ribbon overrun [Issue #3]
 * //TODO ribbon system property exception handling [Issue #4]
 *
 * 2017/01/06.
 */
@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class GalleryThumb extends StackPane{


    {
        System.setProperty("r_fav", "FAV");
        System.setProperty("r_fav_ctext", "white");
        System.setProperty("r_fav_cfront", "darkred");
        System.setProperty("r_fav_cback", "orangered");
    }

    private final Button info;
    private final Button favBtn;
    /*
            Private properties
         */
    private BORDER_STYLE borderStyle = EDGE_BORDER;
    private String defaultImg = "missing-cover-"+borderStyle.get()+"-border.png";
    private ImageView defaultImage = new ImageView();

    private final int COVER_WIDTH = 120;
    private final int COVER_HEIGHT = 180;
    private final int TEXT_HEIGHT = 50;

    /*
        Ribbon tracking
     */
    private final int MAX_RIBBONS_LEFT = 5;
    private final int MAX_RIBBONS_RIGHT = 5;

    private final List<String> ribbons = new ArrayList<>();

    /*
        Exposed properties
    */
    private StringProperty title = new SimpleStringProperty("This is a really really long title");
    private StringProperty origin = new SimpleStringProperty("From this origin");
    private SimpleObjectProperty<Image> image = new SimpleObjectProperty<>(new Image(GalleryThumb.class.getResource(defaultImg).toString()));


    private final RatingFilterBar bar;

    private StringProperty hourglassTooltip = new SimpleStringProperty("Hourglass tooltip");


    private StringProperty warningTooltip = new SimpleStringProperty("Warning tooltip");


    private SimpleBooleanProperty newTitle = new SimpleBooleanProperty(false);

    private SimpleBooleanProperty unreadTitle = new SimpleBooleanProperty(false);

    private SimpleBooleanProperty favTitle = new SimpleBooleanProperty(false);

    public GalleryThumb(){
        defaultImage.imageProperty().bind(image);

        //fixed dimensions
        VBox content = new VBox();
        getChildren().add(content);

        content.setMaxHeight(COVER_HEIGHT+TEXT_HEIGHT);
        setMaxHeight(COVER_HEIGHT+TEXT_HEIGHT);
        setPrefHeight(COVER_HEIGHT+TEXT_HEIGHT);
        content.setMaxWidth(COVER_WIDTH);
        setMaxWidth(COVER_WIDTH);
        setPrefWidth(COVER_WIDTH);

        defaultImage.setFitWidth(COVER_WIDTH);
        defaultImage.setFitHeight(COVER_HEIGHT);

        //put image in StackPane for easy padding/bordering
        StackPane imgStack = new StackPane(defaultImage);
        imgStack.setFocusTraversable(true);
        content.getChildren().add(imgStack);


        //title label, pad text a bit from left and right
        Label titleLb = new Label();
        titleLb.setAlignment(Pos.CENTER_LEFT);
        titleLb.setWrapText(true);
        titleLb.textProperty().bind(title);
        content.getChildren().add(titleLb);

        //origin label, pad text a bit from left and right
        Label originLb = new Label();
        originLb.textProperty().bind(origin);
        content.getChildren().add(originLb);

        //TODO show only tooltip if text is overrun
        //Problem: Label does not know if its text is overrun
        //Workaround: Bind tooltip display to string length
        //title label overflow tooltip
        Tooltip titleTooltip = new Tooltip();
        titleTooltip.textProperty().bind(title);
        titleLb.setTooltip(titleTooltip);

        Tooltip originTooltip = new Tooltip();
        originTooltip.textProperty().bind(origin);
        originLb.setTooltip(originTooltip);

        /*
            FLOATING TOOLTIP ICONS
         */
        //hourglass
        Label l = new Label("");
        l.setAlignment(Pos.CENTER);
        l.setGraphic(new ImageView(GalleryThumb.class.getResource("hourglass.png").toExternalForm()));
        Tooltip hourglassTooltip = new Tooltip();
        hourglassTooltip.textProperty().bind(hourglassTooltipProperty());
        Tooltip.install(l,hourglassTooltip);
        l.setPrefSize(25,25);
        l.visibleProperty().bind(l.managedProperty());
        l.managedProperty().bind(Bindings.isNotEmpty(hourglassTooltipProperty()));


        //warning
        Label warning = new Label("");
        warning.setGraphic(new ImageView(GalleryThumb.class.getResource("warning.png").toExternalForm()));
        warning.setAlignment(Pos.CENTER);
        Tooltip warningTooltip = new Tooltip();
        warningTooltip.textProperty().bind(warningTooltipProperty());
        Tooltip.install(warning,warningTooltip);
        warning.setPrefSize(25,25);
        warning.visibleProperty().bind(warning.managedProperty());
        warning.managedProperty().bind(Bindings.isNotEmpty(warningTooltipProperty()));

        VBox rightIcons = new VBox(5);
        rightIcons.setPadding(new Insets(4,5,5,5));
        rightIcons.setMaxWidth(20);
        rightIcons.setFillWidth(false);
        StackPane.setAlignment(rightIcons,Pos.TOP_RIGHT);
        rightIcons.getChildren().add(warning);
        rightIcons.getChildren().add(l);
        imgStack.getChildren().add(rightIcons);

        /*
            FAV RIBBON
         */
        Ribbon fav = addRibbon("r_fav");
        fav.visibleProperty().bind(favTitleProperty());


        /*
            UNREAD SIDE RIBBON
         */

        UnreadRibbon unreadRibbon = new UnreadRibbon();
        unreadRibbon.setTranslateX(-42);
        unreadRibbon.setTranslateY(72);
        imgStack.getChildren().add(unreadRibbon);
        unreadRibbon.visibleProperty().bind(unreadTitle);

        /*
            NEW SIDE RIBBON
         */

        SideRibbon sr = new SideRibbon();
        sr.setTranslateX(-38);
        sr.setTranslateY(68);
        imgStack.getChildren().add(sr);
        sr.visibleProperty().bind(newTitle);

        /*
            SIDE BOX WITH BUTTONS
         */

        bar = new RatingFilterBar("");
        info = new ImageButton();
        favBtn = new ImageButton();
        HBox buttonsBar = new HBox(favBtn, info);
        buttonsBar.setPadding(new Insets(2));
        buttonsBar.setAlignment(Pos.CENTER);
        VBox buttons = new VBox(buttonsBar,bar);
        buttons.setMaxHeight(50);
        StackPane.setAlignment(buttons, Pos.BOTTOM_CENTER);

        imgStack.getChildren().add(buttons);

        /*
            DEFAULT CSS STYLING
         */
        getStylesheets().add(GalleryThumb.class.getResource( "gallerythumb.css").toExternalForm());
        l.getStyleClass().add("tooltip-icon");
        l.getStyleClass().add("tooltip-hourglass");
        warning.getStyleClass().add("tooltip-icon");
        warning.getStyleClass().add("tooltip-warning");
        getStyleClass().add("gallery-thumb");
        info.getStyleClass().add("info-button");
        favBtn.getStyleClass().add("fav-button");
        bar.getStyleClass().add("gallery-thumb-rating");
        buttonsBar.getStyleClass().add("gallery-thumb-buttons");
        content.getStyleClass().add("gallery-thumb-content");
        imgStack.getStyleClass().add("gallery-thumb-image-container");
        titleLb.getStyleClass().add("gallery-thumb-title");
        originLb.getStyleClass().add("gallery-thumb-origin");

        /*
            FOCUS
         */
        imgStack.setOnMouseClicked(e->imgStack.requestFocus());
        imgStack.setOnTouchPressed(e->imgStack.requestFocus());

        imgStack.setOnMouseEntered(e->{
            bar.setStyle("-fx-opacity: 90%");
            buttonsBar.setStyle("-fx-opacity: 90%");
            bar.setFocusTraversable(false);
        });
        imgStack.setOnMouseExited(e->{
            bar.setStyle("-fx-opacity: 20%");
            buttonsBar.setStyle("-fx-opacity: 20%");
            bar.setFocusTraversable(false);
        });




        /*
            REMOVED ORIGIN FOR NOW
         */
        originLb.setVisible(false);
        originLb.setManaged(false);
    }


    public Ribbon addRibbon(String rid){
        if(ribbons.size() >= MAX_RIBBONS_LEFT){
            //TODO log error
            return null;
        }
        String rText = System.getProperty(rid);
        if(rText!=null){
            try{
                ribbons.add(rid);
                Ribbon r = new Ribbon();
                r.loadStyle(rid);
                //layout
                r.setTranslateX(-COVER_WIDTH/2 + r.getWidthOffset());
                r.setTranslateY(-COVER_HEIGHT/1.5+ ribbons.size()*30);
                getChildren().add(r);
                return r;
            }catch (Exception e){
                //TODO log error
            }
        }

        return null;
    }

    enum BORDER_STYLE {
        NO_BORDER("no"),EDGE_BORDER("edge"),ROUND_BORDER("round");

        public String get() {
            return border;
        }

        private final String border;

        BORDER_STYLE(String border) {
            this.border = border;
        }
    }


    /*
        Needed for scene builder
     */
    public String getTitle() {
        return title.get();
    }

    public StringProperty titleProperty() {
        return title;
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public String getOrigin() {
        return origin.get();
    }

    public StringProperty originProperty() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin.set(origin);
    }

    public Image getImage() {
        return image.get();
    }

    public SimpleObjectProperty<Image> imageProperty() {
        return image;
    }

    public void setImage(Image image) {
        this.image.set(image);
    }

    public Node getImageNode(){ return defaultImage; }

    public boolean isNewTitle() { return newTitle.get(); }

    public SimpleBooleanProperty newTitleProperty() { return newTitle; }

    public void setNewTitle(boolean newTitle) { this.newTitle.set(newTitle); }

    public String getHourglassTooltip() { return hourglassTooltip.get(); }

    public StringProperty hourglassTooltipProperty() { return hourglassTooltip; }

    public void setHourglassTooltip(String hourglassTooltip) { this.hourglassTooltip.set(hourglassTooltip); }

    public String getWarningTooltip() {
        return warningTooltip.get();
    }

    public StringProperty warningTooltipProperty() {
        return warningTooltip;
    }

    public void setWarningTooltip(String warningTooltip) {
        this.warningTooltip.set(warningTooltip);
    }

    public boolean isFavTitle() {
        return favTitle.get();
    }

    public SimpleBooleanProperty favTitleProperty() {
        return favTitle;
    }

    public void setFavTitle(boolean favTitle) {
        this.favTitle.set(favTitle);
    }

    public RatingFilterBar getBar() {
        return bar;
    }

    public Button getInfoButton(){
        return info;
    }

    public Button getFavButton(){
        return favBtn;
    }

    public boolean isUnreadTitle() {
        return unreadTitle.get();
    }

    public SimpleBooleanProperty unreadTitleProperty() {
        return unreadTitle;
    }

    public void setUnreadTitle(boolean unreadTitle) {
        this.unreadTitle.set(unreadTitle);
    }
}
