package com.pettt.hman.fxcomponents.gallery;

import javafx.beans.property.*;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Polygon;

/**
 * 2017
 */
@SuppressWarnings({"FieldCanBeLocal", "unused", "WeakerAccess"})
public class SideRibbon extends Group {
    private static final int WIDTH = 30;

    /*
            Exposed properties
         */
    private BooleanProperty right = new SimpleBooleanProperty(false);
    private StringProperty text = new SimpleStringProperty(" NEW ");
    private ObjectProperty<Paint> textColor = new SimpleObjectProperty<>(Paint.valueOf("white"));
    private ObjectProperty<Paint> frontColor = new SimpleObjectProperty<>(Paint.valueOf("darkred"));
    private ObjectProperty<Paint> backColor = new SimpleObjectProperty<>(Paint.valueOf("darkred"));
    private BooleanProperty layoutManaged = new SimpleBooleanProperty(true);

    public SideRibbon(){
        setRotate(45);
        getStylesheets().add(GalleryThumb.class.getResource( "side-ribbon.css").toExternalForm());

        managedProperty().bind(layoutManaged);

        int size = 10;
        int size2 = 15;
//        setStyle("-fx-background-color: red");
//        setPickOnBounds(false);
        setMouseTransparent(true);


//        setStyle(
//                "-fx-background-color: black;"
//                //"-fx-shape: M 0 0 L 1 1 L 1 0 Z;"
//        );


        //ribbon front
        Polygon frontRibbon = new Polygon();
        frontRibbon.strokeProperty().bind(backColor);
        frontRibbon.fillProperty().bind(frontColor);

        right.addListener((observable, oldValue, newValue) -> {
            frontRibbon.getPoints().clear();
            if(newValue){
                frontRibbon.getPoints().addAll(
                        0., 0.0,
                        72.,0.,
                        62.,10.,
                        10.,10.
                        );
                //frontRibbon.setTranslateX(1);
            }else{
                frontRibbon.getPoints().addAll(0.0, 0.0,
                        5.0*size, 0.0,
                        4.0*size,1.0*size,
                        5.0*size,2.0*size,
                        0.0, 2.0*size);
                frontRibbon.setTranslateX(-1);
            }
        });




        //javafx has no fucking z-order which could be used to put the ribbon to the back
//        backRibbon.getPoints().addAll(0.0,0.0,
//                1.0*size2,0.0,
//                0.0, -0.6*size2);

        Label lb = new Label();
        lb.getStyleClass().add("ribbon-label");
        lb.textProperty().bind(text);
        lb.textFillProperty().bind(textColor);

        //move text away from edge point
//        right.addListener((observable, oldValue, newValue) -> {
//            if(newValue){
//                lb.setTranslateX(4);
//            }else{
//                lb.setTranslateX(-4);
//            }
//        });

        //lb.setPadding(new Insets(0,10,0,10));
        //setPadding(new Insets(0,10,0,10));

        getChildren().addAll(frontRibbon);

        setOpacity(0.9);

        //FIXME workaround for initial ribbon state
        right.set(!right.get());
        right.set(right.get());
    }

    int getWidthOffset() {
        if(!right.get()){
            return WIDTH -10;
        } else{
            return WIDTH +10;
        }
    }

    public void loadStyle(String rid){
        try{
            String rText = System.getProperty(rid);

            Color textColor = Color.valueOf(System.getProperty(rid+"_ctext"));
            Color frontColor = Color.valueOf(System.getProperty(rid+"_cfront"));
            Color backColor = Color.valueOf(System.getProperty(rid+"_cback"));

            this.text.set(rText);
            this.textColor.set(textColor);
            this.frontColor.set(frontColor);
            this.backColor.set(backColor);

            if(System.getProperty(rid+"_flip") == null){
                this.right.set(false);
            }
        }catch (Exception e){
            //TODO exception handling
        }
    }

    /*
        Needed for Scene Builder
     */
    public boolean getRight() {
        return right.get();
    }

    public BooleanProperty rightProperty() {
        return right;
    }

    public void setRight(boolean right) {
        this.right.set(right);
    }

    public String getText() {
        return text.get();
    }

    public StringProperty textProperty() {
        return text;
    }

    public void setText(String text) {
        this.text.set(text);
    }

    public Paint getTextColor() {
        return textColor.get();
    }

    public ObjectProperty<Paint> textColorProperty() {
        return textColor;
    }

    public void setTextColor(Paint textColor) {
        this.textColor.set(textColor);
    }

    public Paint getFrontColor() {
        return frontColor.get();
    }

    public ObjectProperty<Paint> frontColorProperty() {
        return frontColor;
    }

    public void setFrontColor(Paint frontColor) {
        this.frontColor.set(frontColor);
    }

    public Paint getBackColor() {
        return backColor.get();
    }

    public ObjectProperty<Paint> backColorProperty() {
        return backColor;
    }

    public void setBackColor(Paint backColor) {
        this.backColor.set(backColor);
    }

    public boolean isLayoutManaged() {
        return layoutManaged.get();
    }

    public BooleanProperty layoutManagedProperty() {
        return layoutManaged;
    }

    public void setLayoutManaged(boolean layoutManaged) {
        this.layoutManaged.set(layoutManaged);
    }
}