package com.pettt.hman.fxcomponents.progress;

import javafx.application.Application;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 * 2017/01/07.
 */
public class ProgressMenuTest extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {


        BorderPane root = new BorderPane();
        root.setStyle("-fx-pref-width: 400;-fx-pref-height: 400;");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

        StackPane box = new StackPane();
        box.setMaxSize(250,20);

        StackPane boxb  = new StackPane();
        boxb.setMaxSize(250,50);
        boxb.setPrefHeight(50);

        VBox testSamples = new VBox(50);
        testSamples.setAlignment(Pos.CENTER);

        StackPane  requestPane= new StackPane();
        requestPane.setMaxSize(240,90);
        RequestableBox sampleRequest = new RequestableBox(new SimpleIntegerProperty(3),"Active", null);
        requestPane.getChildren().add(sampleRequest);
        testSamples.getChildren().addAll(box,boxb,requestPane);

        root.setCenter(testSamples);


        ProgressMenu menu = new ProgressMenu();
        box.getChildren().add(menu);

        ProgressMenu menu2 = new ProgressMenu();


        boxb.getChildren().add(menu2);

        Button setShowing = new Button("Showing Property");
        setShowing.setOnAction(e-> {
            menu.setPopup(!menu.popupProperty().get());
        });

        Button setActive = new Button("Active Property");
        setActive.setOnAction(e-> {
            menu.activeRequestsProperty().set(50);
            menu.holdRequestsProperty().set(23);
        });

        Button setError = new Button("Error Property");
        setError.setOnAction(e-> {
            menu.errorRequestsProperty().set(50);
            menu.queuedRequestsProperty().set(110);
        });

        HBox top = new HBox(4);
        root.setTop(top);
        top.getChildren().addAll(setShowing,setActive,setError);
    }
}
