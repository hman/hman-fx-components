package com.pettt.hman.fxcomponents.progress;

import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

/**
 * Created by piv-k on 6/18/17.
 */
public class RequestableBox extends HBox {

    RequestableBox(IntegerProperty requests, String name, String resource){
        managedProperty().bind(
                Bindings.when(requests.greaterThan(-1)).then(true).otherwise(false)
        );
        visibleProperty().bind(
                Bindings.when(requests.greaterThan(-1)).then(true).otherwise(false)
        );

        setPrefHeight(25.0);
        setMaxHeight(25.0);
        setSpacing(5);
        setPadding(new Insets(0,2,0,2));

        getStyleClass().add("requestable-box");

        Label title = new Label(name+":");
        if(resource!=null){
            SimpleObjectProperty<Image> image = new SimpleObjectProperty<>(new Image(RequestableBox.class.getResource(resource).toString()));
            title.setGraphic(new ImageView(image.get()));
        }
        getChildren().add(title);


        Label count = new Label("-1");
        count.textProperty().bind(requests.asString());
        count.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(count,Priority.ALWAYS);
        count.setAlignment(Pos.CENTER_RIGHT);

        getChildren().add(count);
    }

    // ******************** Style related *************************************
    @Override public String getUserAgentStylesheet() {
        return RequestableBox.class.getResource("requestablebox.css").toExternalForm();
    }
}
