package com.pettt.hman.fxcomponents.progress;

import javafx.beans.DefaultProperty;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.NumberBinding;
import javafx.beans.property.*;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.event.Event;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.util.Pair;

import java.util.function.BiConsumer;
import java.util.function.Consumer;


@DefaultProperty("children")
public class ProgressMenu extends Region {
    // CSS pseudo classes
    private static final PseudoClass        ERROR_PSEUDO_CLASS    = PseudoClass.getPseudoClass("error");
    private              BooleanProperty    error;
    private static final PseudoClass        ACTIVE_PSEUDO_CLASS   = PseudoClass.getPseudoClass("active");
    private              BooleanProperty    active;
    private static final PseudoClass        POPUP_PSEUDO_CLASS  = PseudoClass.getPseudoClass("popup");
    private              BooleanProperty    popup;

    // View related
    private static final double             SCALE            = 5;
    private static final double             PREFERRED_HEIGHT = 30;
    private static final double             PREFERRED_WIDTH  = PREFERRED_HEIGHT*SCALE;
    private static final double             MINIMUM_HEIGHT   = 30;
    private static final double             MINIMUM_WIDTH    = MINIMUM_HEIGHT*SCALE;
    private static final double             MAXIMUM_HEIGHT   = 50;
    private static final double             MAXIMUM_WIDTH    = MAXIMUM_HEIGHT*SCALE;

    private static final String          ACTIVE         = "ic_compare_arrows_white_18dp_1x.png";


    private              IntegerProperty activeRequests = new SimpleIntegerProperty(-1);
    private static final String          QUEUED         = "ic_hourglass_empty_white_18dp_1x.png";
    private              IntegerProperty queuedRequests = new SimpleIntegerProperty(-1);
    private static final String          HOLD           = "ic_brightness_3_white_18dp_1x.png";
    private              IntegerProperty holdRequests   = new SimpleIntegerProperty(-1);
    private static final String          ERROR          = "ic_error_white_18dp_1x.png";
    private              IntegerProperty errorRequests  = new SimpleIntegerProperty(-1);
    private              IntegerProperty activeComponents = new SimpleIntegerProperty(0);


    private              Pane               iconPane;
    private              Pane             description;
    private              Pane               indicator;
    private              Circle             indicatorCircle;
    private              StackPane          tick;
    private              Pane             warning;
    private Label l;
    private Label desc;
    private Pane popupPane;
    private Pane buttonPane;


    // ******************** Constructors **************************************
    public ProgressMenu() {
        error    = new BooleanPropertyBase(false) {
            @Override protected void invalidated() { pseudoClassStateChanged(ERROR_PSEUDO_CLASS, get()); }
            @Override public Object getBean() { return this; }
            @Override public String getName() { return "error"; }
        };
        active    = new BooleanPropertyBase(false) {
            @Override protected void invalidated() { pseudoClassStateChanged(ACTIVE_PSEUDO_CLASS, get()); }
            @Override public Object getBean() { return this; }
            @Override public String getName() { return "active"; }
        };
        popup = new BooleanPropertyBase(false) {
            @Override protected void invalidated() { pseudoClassStateChanged(POPUP_PSEUDO_CLASS, get()); }
            @Override public Object getBean() { return this; }
            @Override public String getName() { return "popup"; }
        };

        initGraphics();
        registerListeners();
    }



    // ******************** Initialization ************************************
    private void initGraphics() {
        // Set initial size
        if (Double.compare(getWidth(), 0) <= 0 || Double.compare(getHeight(), 0) <= 0 ||
                Double.compare(getPrefWidth(), 0) <= 0 || Double.compare(getPrefHeight(), 0) <= 0) {
            setPrefSize(PREFERRED_WIDTH, PREFERRED_HEIGHT);
        }

        // Apply the base CSS style class to the control
        getStyleClass().setAll("progress-menu-top");

        buttonPane = new Pane();
        buttonPane.getStyleClass().setAll("progress-menu");


        // active/done progress indicator space
        iconPane = new Pane();
        iconPane.getStyleClass().setAll("icon-pane");

        ProgressIndicator p = new ProgressIndicator(-1);
        indicator = new Pane();
        indicator.getStyleClass().setAll("spinner");
        indicator.setScaleShape(false);
        indicator.setCenterShape(false);
        indicatorCircle = new Circle();
        indicator.setShape(indicatorCircle);
        p.prefWidthProperty().bind(iconPane.widthProperty());
        p.prefHeightProperty().bind(iconPane.heightProperty());
        iconPane.getChildren().add(p);

        tick = new StackPane();
        tick.getStyleClass().setAll("tick");

        //description and warning space
        description = new StackPane();
        description.getStyleClass().setAll("description");

        desc = new Label("Done.");
        StackPane.setAlignment(desc, Pos.CENTER_LEFT);
        tick.getStyleClass().add("description-label");
        description.getChildren().addAll(desc);


        warning = new StackPane();
        warning.getStyleClass().setAll("warning");

        l = new Label("!");
        l.setStyle("-fx-font-size: "+getHeight());
        warning.getChildren().addAll(l);

        //list popupRegion
        popupPane = new StackPane();
        popupPane.getStyleClass().setAll("popup");

        //popupPane.getChildren().add(new Label("No Active Tasks"));

        VBox popupVbox = new VBox(5);
        popupPane.getChildren().add(popupVbox);

        popupVbox.getChildren().add(new RequestableBox(activeRequests,"Active", ACTIVE));
        popupVbox.getChildren().add(new RequestableBox(queuedRequests,"Queued",QUEUED));
        popupVbox.getChildren().add(new RequestableBox(holdRequests,"Hold", HOLD));
        popupVbox.getChildren().add(new RequestableBox(errorRequests,"Errors", ERROR));

        // Add all nodes
        buttonPane.getChildren().addAll(iconPane, description,indicator,tick,warning);

        getChildren().addAll(buttonPane,popupPane);
    }

    private void registerListeners() {
        activeComponents.bind(
                Bindings.
                add(0,Bindings.when(activeRequests.greaterThan(-1)).then(1).otherwise(0)).
                add(Bindings.when(holdRequests.greaterThan(-1)).then(1).otherwise(0)).
                add(Bindings.when(queuedRequests.greaterThan(-1)).then(1).otherwise(0)).
                add(Bindings.when(errorRequests.greaterThan(-1)).then(1).otherwise(0))
        );

        NumberBinding sum = Bindings.
                add(0, Bindings.when(activeRequests.greaterThan(-1)).then(activeRequests).otherwise(0)).
                add(Bindings.when(holdRequests.greaterThan(-1)).then(holdRequests).otherwise(0)).
                add(Bindings.when(queuedRequests.greaterThan(-1)).then(queuedRequests).otherwise(0))//.
                //add(Bindings.when(errorRequests.greaterThan(-1)).then(errorRequests).otherwise(0))
        ;

        desc.textProperty().bind(Bindings.when(sum.isEqualTo(0)).then("Done.").otherwise(sum.asString().concat(" pending")));



        activeComponents.addListener(o -> handleControlPropertyChanged("RESIZE"));
        widthProperty().addListener(o -> handleControlPropertyChanged("RESIZE"));
        heightProperty().addListener(o -> handleControlPropertyChanged("RESIZE"));
        errorProperty().addListener(o->handleControlPropertyChanged("ERROR"));
        popupProperty().addListener(o->handleControlPropertyChanged("POPUP"));
        activeProperty().addListener(o->handleControlPropertyChanged("ACTIVE"));

        // bind visibility
        tick.visibleProperty().bind(activeProperty().not());
        indicator.visibleProperty().bind(activeProperty().not());
        iconPane.visibleProperty().bind(activeProperty());
        warning.visibleProperty().bind(errorProperty());
        popupPane.visibleProperty().bind(
                Bindings.when(
                        activeComponents.greaterThan(0).
                                and(popupProperty())).then(true).otherwise(false)
        );

        errorProperty().bind(
                Bindings.when(errorRequests.greaterThan(0)).then(true).otherwise(false)
        );

        activeProperty().bind(
                Bindings.when(activeRequests.greaterThan(0)).then(true).otherwise(false)
        );


        setOnMouseEntered(e -> showPopup(true));
        setOnMouseExited(e -> showPopup(false));
    }


    // ******************** Methods *******************************************
    @Override public void layoutChildren() {
        super.layoutChildren();
    }

    @Override protected double computeMinWidth(final double HEIGHT)  { return MINIMUM_WIDTH; }
    @Override protected double computeMinHeight(final double WIDTH)  { return MINIMUM_HEIGHT; }
    @Override protected double computePrefWidth(final double HEIGHT) { return super.computePrefWidth(HEIGHT); }
    @Override protected double computePrefHeight(final double WIDTH) { return super.computePrefHeight(WIDTH); }
    @Override protected double computeMaxWidth(final double HEIGHT)  { return MAXIMUM_WIDTH; }
    @Override protected double computeMaxHeight(final double WIDTH)  { return MAXIMUM_HEIGHT; }

    @SuppressWarnings("StatementWithEmptyBody")
    protected void handleControlPropertyChanged(final String PROPERTY) {
        if ("RESIZE".equals(PROPERTY)) {
            resize();
        } else if ("ACTIVE".equals(PROPERTY)) {
            buttonPane.pseudoClassStateChanged(ACTIVE_PSEUDO_CLASS,active.get());
        } else if ("ERROR".equals(PROPERTY)) {
            buttonPane.pseudoClassStateChanged(ERROR_PSEUDO_CLASS,error.get());
        } else if ("POPUP".equals(PROPERTY)) {
            buttonPane.pseudoClassStateChanged(POPUP_PSEUDO_CLASS,popup.get());
        }
    }

    public int getActiveRequests() { return activeRequests.get(); }
    public IntegerProperty activeRequestsProperty() { return activeRequests; }
    public void setActiveRequests(int activeRequests) { this.activeRequests.set(activeRequests); }

    public int getQueuedRequests() { return queuedRequests.get(); }
    public IntegerProperty queuedRequestsProperty() { return queuedRequests; }
    public void setQueuedRequests(int queuedRequests) { this.queuedRequests.set(queuedRequests); }

    public int getHoldRequests() { return holdRequests.get(); }
    public IntegerProperty holdRequestsProperty() { return holdRequests; }
    public void setHoldRequests(int holdRequests) { this.holdRequests.set(holdRequests); }

    public int getErrorRequests() { return errorRequests.get(); }
    public IntegerProperty errorRequestsProperty() { return errorRequests; }
    public void setErrorRequests(int errorRequests) { this.errorRequests.set(errorRequests); }

    public boolean isError() { return error.get(); }
    public BooleanProperty errorProperty() { return error; }
    public void setError(boolean error) { this.error.set(error); }

    public boolean isActive() { return active.get(); }
    public BooleanProperty activeProperty() { return active; }
    public void setActive(boolean active) { this.active.set(active); }

    public boolean isPopup() { return popup.get(); }
    public BooleanProperty popupProperty() { return popup; }
    public void setPopup(boolean showing) { this.popup.set(showing); }

    @Override public ObservableList<Node> getChildren() { return super.getChildren(); }

    // ******************** Style related *************************************
    @Override public String getUserAgentStylesheet() {
        return ProgressMenu.class.getResource("progressmenu.css").toExternalForm();
    }

    // ******************** Resizing ******************************************
    private void resize() {
        Insets padding = buttonPane.getInsets();

        double innerWidth  = getWidth() - padding.getLeft() - padding.getRight();
        double innerHeight = getHeight() - padding.getTop() - padding.getTop();


        buttonPane.setPrefWidth(getWidth());
        buttonPane.setPrefHeight(getHeight());

        // ************ ICON **********************
        //20% of inner width, box
        double iconWidth = innerWidth*0.2;
        double iconHeight = innerHeight;
        // calculate extra translate x if too much width
        double extraWidth = 0;
        if(iconWidth>iconHeight){
            extraWidth = iconWidth-iconHeight;
        }
        iconPane.setPrefHeight(iconWidth);
        iconPane.setPrefWidth(iconHeight);
        iconPane.setMaxSize(iconWidth,iconHeight);
        //center
        iconPane.setTranslateY(padding.getTop());
        iconPane.setTranslateX(padding.getLeft() + extraWidth/2);

        // ************ DESCRIPTION ***************
        //80% of width
        double descriptionHeight = innerHeight;
        double descriptionWidth = (innerWidth/5)*4;
        description.setPrefHeight(descriptionHeight);
        description.setPrefWidth(descriptionWidth);
        //move right
        description.setTranslateX(padding.getLeft() + iconWidth);
        description.setTranslateY((padding.getTop() + padding.getBottom())/2);

        // *********** Circle *********************
        double r = iconHeight/2;

        indicatorCircle.setRadius(r);
        indicator.setTranslateX(r+padding.getLeft() + extraWidth/2);
        //move circle to middle + center
        indicator.setTranslateY((padding.getTop() + padding.getBottom())/2+r );

        // ********** Tick ***********************
        double tickSize = 16.0; //hard-coded in css file (2xpadding)

        tick.setLayoutX(padding.getLeft() + r -tickSize/2 + extraWidth/2);
        tick.setLayoutY(padding.getTop() + r - tickSize/2);

        // ********** Warning *******************
        //warning.setTranslateX(0);
        //warning.setTranslateY(0);
        warning.setTranslateX(padding.getLeft() + iconWidth*0.8);
        warning.setTranslateY(padding.getTop());
        l.setStyle("-fx-font-size: "+iconHeight*0.75);

        // ************ POPUP *********************
        //100% of width
        popupPane.setPadding(new Insets(2,2,4,2));
        popupPane.setPrefWidth(getWidth());
        //move down
        int h = activeComponents.get() -1;
        if(h < 0) h = 0;

        popupPane.setTranslateY(-(25*activeComponents.get()) - 2 - h*5 - 4);
    }

    public void showPopup(boolean show){
        popup.set(show);
    }


}

