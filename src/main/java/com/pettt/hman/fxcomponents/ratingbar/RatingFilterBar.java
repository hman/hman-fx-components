package com.pettt.hman.fxcomponents.ratingbar;

import com.pettt.hman.fxcomponents.imagebutton.ImageButton;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import org.controlsfx.control.Rating;

//TODO [LOW] JavaDoc RatingFilterBar
/**
 * Rating bar with optional reset to zero button.
 * <p>
 * 2016/03/25.
 *
 * @see Rating
 */
public class RatingFilterBar extends HBox {
    private FloatProperty ratingProperty = new SimpleFloatProperty(0);

    /** Default constructor with reset button */
    public RatingFilterBar(String text){
        this(text,true);
    }

    public RatingFilterBar(String text, boolean ratingReset){
        //HBox style;
        setAlignment(Pos.CENTER);
        setSpacing(5);


        //rating
        final Rating rating = new Rating();
        rating.setPartialRating(true);
        ratingProperty.bindBidirectional(rating.ratingProperty());

        //why doesn't Rating have a resize/star size option holy shit
        //start size hack
        this.setMaxHeight(25);
        this.setMinHeight(25);
        rating.setScaleX(0.5);
        rating.setScaleY(0.5);
        final Group zoomContent = new Group(rating);
        final StackPane ratingPane = new StackPane();
        ratingPane.getChildren().add(zoomContent);
        ratingPane.setMinSize(100, 15);
        ratingPane.setMaxSize(100, 15);
        //end size hack



        //label
        final Label label = new Label(text);
        label.setStyle("-fx-font-size:15px;-fx-text-fill: white;");

        getChildren().add(label);
        getChildren().add(zoomContent);

//        //reset rating button if needed
//        if(ratingReset) {
            Button reset = new ImageButton();
            reset.getStyleClass().add("ratingbar-reset");
            reset.setOnAction(e -> ratingProperty().set(0));
            getChildren().add(reset);
//        }

    }

    /** Rating property of the rating bar */
    public FloatProperty ratingProperty() {
        return ratingProperty;
    }
}
