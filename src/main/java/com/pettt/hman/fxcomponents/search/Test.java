package com.pettt.hman.fxcomponents.search;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * 2017/01/05.
 */
public class Test extends Application{
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            VBox root = new VBox();
            Scene scene = new Scene(root,400,400);
            primaryStage.setScene(scene);

            root.setAlignment(Pos.CENTER);


            //AutoCompleteTextField gt = new AutoCompleteTextField();

            AdvancedSearchField gt = new AdvancedSearchField();

            root.getChildren().add(gt);




            primaryStage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}
