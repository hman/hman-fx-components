package com.pettt.hman.fxcomponents.search;

import com.pettt.hman.fxcomponents.imagebutton.ImageButton;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.util.SortedSet;

public class AdvancedSearchField extends AnchorPane {
    AutoCompleteTextField tf;
    Button advancedSearch;

    public AdvancedSearchField(){
        super();
        getStylesheets().add(AdvancedSearchField.class.getResource( "advancedsearch.css").toExternalForm());

        tf = new AutoCompleteTextField();
        AnchorPane.setLeftAnchor(tf,0.);
        AnchorPane.setRightAnchor(tf,0.);
        AnchorPane.setTopAnchor(tf,0.);
        AnchorPane.setBottomAnchor(tf,0.);

        tf.getStyleClass().add("search");

        getChildren().add(tf);


        advancedSearch = new ImageButton("");
        advancedSearch.getStyleClass().add("search-button");
        getChildren().add(advancedSearch);

        AnchorPane.setRightAnchor(advancedSearch,0.);
        AnchorPane.setTopAnchor(advancedSearch,0.);
        AnchorPane.setBottomAnchor(advancedSearch,0.);

        advancedSearch.setOnAction(e -> tf.fireEvent(new ActionEvent()));

    }

    public SortedSet<String> getEntries(){
        return tf.getEntries();
    }

    public AutoCompleteTextField getTextField() {
        return tf;
    }

    public Button getButton() {
        return advancedSearch;
    }
}

