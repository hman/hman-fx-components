package com.pettt.hman.fxcomponents.imagebutton;

import javafx.scene.control.Button;

/** Created by piv-k on 7/11/17. */
public class ImageButton extends Button{

    public ImageButton(){
        this("");
    }

    public ImageButton(String settings){
        super(settings);
        getStylesheets().add(ImageButton.class.getResource("imagebutton.css").toString());

        getStyleClass().add("image-button");
    }
}
