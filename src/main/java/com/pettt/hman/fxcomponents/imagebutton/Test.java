package com.pettt.hman.fxcomponents.imagebutton;


import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/** Created by piv-k on 7/11/17. */
public class Test extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        StackPane p = new StackPane();
        p.setStyle("-fx-background-color: grey");

        VBox b = new VBox(5);

        b.setMaxWidth(400);
        b.setMaxHeight(400);
        p.getChildren().add(b);

        b.getChildren().add(new ImageButton("Settings"));
        b.getChildren().add(new ImageButton("History"));




        Scene sc = new Scene(p);
        primaryStage.setScene(sc);
        primaryStage.show();
    }
}
